<?php
/**
 * Interface CourierInterface
 */
interface CourierInterface
{

    /**
     * Invokes GetId()
     *  Grabs the ID from the courier-specific algorithm.
     *
     * @return int
     */
    public function getId();

    /**
     * Invokes dispatch()
     *  Dispatches ID to courier (using method outlined in spec).
     *
     * @param $id
     * @return int
     */
    public function dispatch($id);

}
