<?php
require_once '../Interfaces/Courier.php';

class AncCourier implements CourierInterface
{
    /**
     * @var int
     */
    private static $i = 0;

    /**
     * @inheritdoc
     */
    public function getID()
    {
        // Do algorithm stuff, then return
        return self::$i;
    }

    /**
     * @inheritdoc
     */
    public function dispatch($number)
    {
        // Dispatch to courier standard.
    }
}
