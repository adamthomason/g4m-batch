<?php

/**
 * Class Batch
 */
class Batch
{
    /**
     * Batch constructor.
     */
    public function __construct()
    {
        $GLOBALS['batch_running'] = false;
    }

    /**
     * Invokes start_batch().
     *  Begins the batch.
     */
    public function start_batch()
    {
       if ($GLOBALS['batch_running'] == false) {
           $GLOBALS['batch_running'] = true;
       }
       else {
           // Do nothing
       }
    }

    /**
     * Invokes addConsignment().
     *  Adds a consignment to a courier.
     *
     * @param Consignment $consignment
     */
    public function addConsignment(Consignment $consignment)
    {
        if ($GLOBALS['batch_running'] == true) {
            $consignment->execute();
        }
        else {
            // Do nothing
        }
    }

    /**
     * Invokes stop_batch().
     *  Stops the batch.
     */
    public function stop()
    {
        if ($GLOBALS['batch_running'] == true) {
            $GLOBALS['batch_running'] = false;
        }
        else {
            // Do nothing
        }
    }
}
