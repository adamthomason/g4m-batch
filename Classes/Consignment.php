<?php

/**
 * Class Consignment
 */
class Consignment
{

    /**
     * @var CourierInterface
     */
    public $courier;

    /**
     * @var int
     */
    protected $id = 0;

    /**
     * Invokes Consignment constructor.
     *  Takes courier as parameter.
     *
     * @param CourierInterface $courier
     */
    public function __construct(CourierInterface $courier)
    {
        $this->courier = $courier;
        $this->id = $this->courier->getId();
    }

    /**
     * Invokes getId().
     *  Grabs the Consignment ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Invokes execute().
     *  Passes consignment ID through to dispatch
     *
     * @return int
     */
    public function execute()
    {
        return $this->courier->dispatch($this->id);
    }

}